package com.btc.withings.service.workflow;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class JobsScheduler {
	private static final Logger logger = LogManager.getLogger(JobsScheduler.class);
		/*
		 * 
		 * */
	
	@Scheduled(cron = "0 0/30 * 1/1 * ? *")
	public void do30Minutes() {
	      System.out.println("30 minutes Jobs Schduler tick");
	   }
	
	@Scheduled(cron = "0 0/10 * 1/1 * ? *") //every 10 minutes
	   public void do20Minutes() {
	      System.out.println("10 minutes Jobs Schduler tick");
	   }

}
