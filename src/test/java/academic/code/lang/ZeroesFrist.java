package academic.code.lang;

public class ZeroesFrist {

	public static void main(String[] args) {
		new ZeroesFrist().testCases();
	}

	/*
	 * Return an array that contains the exact same numbers as the given array, but rearranged so that all the zeros are grouped at the start of the
	 * array. The order of the non-zero numbers does not matter. So {1, 0, 0, 1} becomes {0 ,0, 1, 1}. You may modify and return the given array or
	 * make a new array.
	 *
	 * zeroFront([1, 0, 0, 1]) → [0, 0, 1, 1]
	 * zeroFront([0, 1, 1, 0, 1]) → [0, 0, 1, 1, 1]
	 * zeroFront([1, 0]) → [0, 1]
	 *
	 * No array list or anything other than java.lang for this problem. Hint : keep track & set numbers from both ends
	 *
	 * Solve with main method, test helper and test cases like in https://github.com/tgkprog/codingbat/blob/master/src/org/moh/academic/IcyHot.java
	 */

	// signature should not change
	public int[] zeroFront(int[] nums) {
		int rtn[] = new int[nums.length];
		var zeroAt = 0;
		var otherNumberAt = nums.length - 1;
		for (var i = 0; i < nums.length; i++) {
			if (nums[i] == 0) {
				rtn[zeroAt] = 0;
				zeroAt++;
			} else {
				rtn[otherNumberAt] = nums[i];
				otherNumberAt--;
			}
		}
		return rtn;
	}

	private int count;
	private int errs;

	public void testZeroFirst(int[] nums, int[] expected) {
		count++;
		int[] actual = null;
		try {
			actual = zeroFront(nums);
			if (actual == null || expected == null) {
				System.out.println("Err in test case " + count + " got null actual " + actual + ", expected " + expected);
				printAr(actual);
				errs++;
				return;
			}
			if (actual.length != expected.length) {
				System.out.println("Err in in test case " + count + " got size actual len :" + actual.length + ", expected len " + expected.length);
				printAr(actual);
				errs++;
				return;
			}
			var zeros = true;
			var cntZerosActual = 0;
			var cntZerosExp = 0;
			for (var i = 0; i < actual.length; i++) {
				if (actual[i] == 0) {
					cntZerosActual++;
					if (!zeros) {

						System.out.println(
								"Err in in test case " + count + " zero after number at index, i :" + i);
						printAr(actual);
						errs++;
						return;

					}
				} else {

					zeros = false;
				}
				if (expected[i] == 0) {
					cntZerosExp++;
				}
			}
			if (cntZerosExp != cntZerosActual) {
				System.out.println(
						"Err in in test case " + count + " zero count, expecting :" + cntZerosExp + ", got " + cntZerosActual);
				printAr(actual);
				errs++;
				return;

			}
//			for (var i = 0; i < actual.length; i++) {
//				if (actual[i] != expected[i]) {
//					System.out.println(
//							"Err in in test case " + count + " for int at index :" + i + ", expecting :" + expected[i] + ", got " + actual[i]);
//					errs++;
//					return;
//
//				}
//			}
		} catch (Exception e) {
			System.out.println("Err in test case " + count + " got null actual " + actual + ", expected " + expected);
			e.printStackTrace();
			errs++;
		}
	}

	private void printAr(int[] ar) {
		System.out.println("print ar ");
		for (var i = 0; i < ar.length; i++) {
			System.out.print(ar[i] + ", ");
		}
		System.out.println(".");

	}

	public void testCases() {
		System.out.println("Starting test cases , run at " + new java.util.Date());
		testZeroFirst(new int[] { 1, 0, 0, 1 }, new int[] { 0, 0, 1, 1 });
		testZeroFirst(new int[] { 1, 4, 0, 0, 3, 0, 7 }, new int[] { 0, 0, 0, 7, 3, 4, 1 });
		testZeroFirst(new int[] { 0, 0, 0, 1 }, new int[] { 0, 0, 0, 1 });
		testZeroFirst(new int[] { -100, 8999, 333, 1 }, new int[] { -100, 8999, 333, 1 });
		testZeroFirst(new int[] { -100, 0, 8999, 333, 1 }, new int[] { 0, 1, 333, 8999, -100 });

		System.out.println("Zeros first test cases count " + count
				+ ", Errors (test case expected value wrong or implmentaion wrong or problem understanding wrong):" + errs + ".");
		if (errs == 0) {
			System.out.println(" Yay No errors ! :-) ***");
		}

	}

}
