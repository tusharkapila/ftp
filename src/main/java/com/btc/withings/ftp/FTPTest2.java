package com.btc.withings.ftp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.vfs2.provider.ftps.FtpsMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.btc.withings.config.FtpConfig;
import com.btc.withings.utils.PrintCommandToLoggerListener;

@Service
public class FTPTest2 implements ApplicationListener<ApplicationReadyEvent>, Runnable {

	private static final Logger logger = LogManager.getLogger(FTPTest2.class);

	@Autowired
	private FtpConfig ftpConfig;

	private PrintCommandToLoggerListener printFtpCmdsToLog = new PrintCommandToLoggerListener(logger);

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("ApplicationListener#onApplicationEvent FTPTest2 () v4");
		Thread t = new Thread(this);// only for testing
		t.start();
		com.btc.withings.ftp.FTPTest.slpAndExit();
	}

	@Override
	public void run() {

		var fl = new File(new File("/d/data/wi/prod1/d/data/wi/ftp/out/1010000001/21_10/"),
				"XPORT-MF-035_PROD_Withings_WeightMonitoring_INCR_2021-10-20_17_01_31_724_1010000003.csv");
		sendFile(fl);

	}

	public boolean sendFile(File fl) {

		final String ftpServer = ftpConfig.getHost();
		final String ftpUsername = ftpConfig.getUsername();
		final String ftpPassword = ftpConfig.getPassword();
		final int port = ftpConfig.getPort();

		FTPSClient ftp = null;
		InputStream fis = null;
		logger.info("Ftp send file {}", fl);
		boolean loggedIn = false;
		try {
			if (!fl.exists() || !fl.canRead() || fl.length() <= 0) {
				logger.info("File does not exist {} or cant read {} or length is <= 0 ", fl.exists(), fl.canRead(), fl.length());
				return false;
			}

			ftp = new FTPSClient();
			ftp.addProtocolCommandListener(new PrintCommandListener(printFtpCmdsToLog.getPrintWriter(), true));// to log to log4j
			var duraI = 70 * 1000;
			try {
				ftp.setConnectTimeout(duraI);
				ftp.setDataTimeout(duraI);
				ftp.setDefaultTimeout(duraI);
			} catch (Exception e) {
				logger.info("2ve err setConnectTimeout ", e);

			}
			ftp.connect(ftpServer, port);
			try {
				ftp.setSoTimeout(duraI);
			} catch (Exception e) {
				logger.info("2ve err setSoTimeout ", e);

			}
			logger.info("new fle {}", fl);
			int reply = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				logger.warn("coult not connect to ftp server reply : {}", reply);
				return false;
			}

			ftp.enterLocalPassiveMode();// Run the passive mode command now instead of after loggin in.
			ftp.login(ftpUsername, ftpPassword);
			loggedIn = true;
			ftp.execPBSZ(0);
			ftp.execPROT("P");
			ftp.type(FTP.BINARY_FILE_TYPE);
			if (ftpConfig.getRemoteDir() != null && ftpConfig.getRemoteDir().length() > 0) {
				reply = ftp.cwd(ftpConfig.getRemoteDir());
				if (!FTPReply.isPositiveCompletion(reply)) {
					logger.warn("coult not cwd to  : {}, reply :", ftpConfig.getRemoteDir(), reply);
					return false;
				}
			}

			var rootUri = ftpConfig.getProtocol() + "://" + ftpConfig.getUsername() + ":" + ftpConfig.getPassword() + "@" + ftpConfig.getHost() + ""
					+ ftpConfig.getRemoteDir();
			var fileS = rootUri;
			logger.info("rootUri :{}, full File :{}. FtpsMode.EXPLICIT {}", rootUri, fileS, FtpsMode.EXPLICIT);
			fis = new BufferedInputStream(new FileInputStream(fl));
			logger.info("storeFile");
			ftp.storeFile(fl.getName(), fis);// this calls completePendingCommand, no need to call it again
			logger.info("uploaded");
			return true;
		} catch (Exception e) {
			logger.error("Sftp err " + e, e);
			return false;
		} finally {
			if (ftp != null & loggedIn) {
				try {
					logger.info("ftp logout finally");
					ftp.logout();
				} catch (Exception e) {
					logger.error("ftp logout err ", e);
				}
				try {
					logger.info("ftp disconnect finally");
					ftp.disconnect();
				} catch (Exception e) {
					logger.error("Sftp local close ", e);
				}
			}
			if (fis != null) {
				try {
					logger.info("fis close 4v");
					fis.close();

				} catch (Exception e) {
					logger.error("fis close 4 err ", e);
				}
			}

		}

	}

}
