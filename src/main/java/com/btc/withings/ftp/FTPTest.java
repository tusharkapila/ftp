package com.btc.withings.ftp;

import java.io.File;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.ftp.FtpFileType;
import org.apache.commons.vfs2.provider.ftps.FtpsFileSystemConfigBuilder;
import org.apache.commons.vfs2.provider.ftps.FtpsMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.btc.withings.config.FtpConfig;

@Service
public class FTPTest implements ApplicationListener<ApplicationReadyEvent> {
	@Autowired
	private FtpConfig ftpConfig;

	private static final Logger logger = LogManager.getLogger(FTPTest.class);

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("a-ApplicationListener#onApplicationEventFTPTest()");
		if (ftpConfig.isTryUpload()) {
			doFtpOnApplicationEvent(event);
		} else {
			logger.info("a- f1 off");
		}
		com.btc.withings.ftp.FTPTest.slpAndExit();

	}

	public void doFtpOnApplicationEvent(ApplicationReadyEvent event) {
		FileObject remote = null;
		FileObject local = null;
		org.apache.commons.vfs2.impl.DefaultFileSystemManager manager = null;
		try {
			manager = new StandardFileSystemManager();
			logger.info("ftp A 1");
			manager.init();
			var fl = new File(new File("/d/tushar/www/"), "tst.txt");
			local = manager.resolveFile(fl.getAbsolutePath());
			logger.info("2");
			var builder = FtpsFileSystemConfigBuilder.getInstance();

			var fileSystemOptions = new FileSystemOptions();
			var duraI = 70;
			var duration = Duration.of(duraI, ChronoUnit.SECONDS);
			builder.setConnectTimeout(fileSystemOptions, duration);
			builder.setUserDirIsRoot(fileSystemOptions, true);
			builder.setDataTimeout(fileSystemOptions, duration);

			builder.setSoTimeout(fileSystemOptions, duration);

			builder.setFtpsMode(fileSystemOptions, FtpsMode.EXPLICIT);
			builder.setPassiveMode(fileSystemOptions, true);

			/// builder.setSessionTimeout(fileSystemOptions, 35000);

			builder.setFileType(fileSystemOptions, FtpFileType.BINARY);
			// builder.setFileNameEncoding(fileSystemOptions, "UTF-8");
			var rootUri = ftpConfig.getProtocol() + "://" + ftpConfig.getUsername() + ":" + ftpConfig.getPassword() + "@" + ftpConfig.getHost() + ""
					+ ftpConfig.getRemoteDir();
			// builder.setStrictHostKeyChecking(fileSystemOptions, "no");
			// builder.setIdentityRepositoryFactory(fileSystemOptions, new TestIdentityRepositoryFactory());
			// builder.setUserInfo(fileSystemOptions, new TrustEveryoneUserInfo());
			builder.setRootURI(fileSystemOptions, rootUri);
			var fileS = rootUri;
			logger.info("rootUri :{}, full File :{}. FtpsMode.EXPLICIT {}", rootUri, fileS, FtpsMode.EXPLICIT);
			remote = manager.resolveFile(fileS, fileSystemOptions);
			logger.info("3");
			// logger.info(" remote.getType() {}", remote.getType());
			// remote.getType()
			// logger.info("3a");
//			var i = 0;
//			var rmChildren = remote.getChildren();
//			logger.info("3b");
//			for (var chld : rmChildren) {
//				i++;
//				logger.info("child {} type {} , name {}", i, chld.getType(), chld.getName());
//			}

			logger.info("attempt copy");
			remote.copyFrom(local, Selectors.SELECT_SELF);

			logger.info("uploaded");
		} catch (FileSystemException e) {
			e.printStackTrace();
			logger.error("Sftp test " + e, e);
		} finally {
			try {
				logger.info("local close");
				local.close();
			} catch (FileSystemException e) {
				logger.error("Sftp test local close ", e);
			}
			if (remote != null) {
				try {

					logger.info("5");
					remote.close();

				} catch (FileSystemException e) {
					logger.error("Sftp test remote close ", e);
				}
			}
			if (manager != null) {
				try {
					manager.close();
				} catch (Exception e) {
					logger.error("Sftp test mgr close ", e);
				}

			}
		}

	}

	public static void slpAndExit() {
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(75000);
					logger.info("slep 75");
					Thread.sleep(13000);
					logger.info("slept 13 exitnow");
					System.exit(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
	}
}
