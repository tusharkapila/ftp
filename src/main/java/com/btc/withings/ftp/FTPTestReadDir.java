package com.btc.withings.ftp;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.ftp.FtpFileType;
import org.apache.commons.vfs2.provider.ftps.FtpsFileSystemConfigBuilder;
import org.apache.commons.vfs2.provider.ftps.FtpsMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.btc.withings.config.FtpConfig;

@Service
public class FTPTestReadDir implements ApplicationListener<ApplicationReadyEvent> {
	@Autowired
	private FtpConfig ftpConfig;

	private static final Logger logger = LogManager.getLogger(FTPTestReadDir.class);

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("read FTPTestReadDir ApplicationListener#onApplicationEvent()");
		FileObject remote = null;
		FileObject local = null;
		if (!ftpConfig.isTryList()) {
			logger.info("TryList false so bye");
			return;
		}
		try {
			var manager = new StandardFileSystemManager();

			var log = LogFactory.getLog(manager.getClass().getCanonicalName());
			log.debug("msg ");
			logger.info("other logs debug? " + log.isDebugEnabled());

			manager.setLogger(log);
			logger.info("read 1");
			manager.init();

			logger.info("read 2");
			var builder = FtpsFileSystemConfigBuilder.getInstance();

			var fileSystemOptions = new FileSystemOptions();
			var duraI = 125;
			var duration = Duration.of(duraI, ChronoUnit.SECONDS);
			builder.setConnectTimeout(fileSystemOptions, duration);
			builder.setUserDirIsRoot(fileSystemOptions, true);
			builder.setDataTimeout(fileSystemOptions, duration);
			builder.setFtpsMode(fileSystemOptions, FtpsMode.EXPLICIT);
			builder.setPassiveMode(fileSystemOptions, true);
			builder.setSoTimeout(fileSystemOptions, duration);
			builder.setSoTimeout(fileSystemOptions, duration);

			// builder.setSessionTimeout(fileSystemOptions, 35000);

			builder.setFileType(fileSystemOptions, FtpFileType.BINARY);
			// builder.setFileNameEncoding(fileSystemOptions, "UTF-8");
			var rootUri = ftpConfig.getProtocol() + "://" + ftpConfig.getUsername() + ":" + ftpConfig.getPassword() + "@" + ftpConfig.getHost() + ""
					+ ftpConfig.getRemoteDir();
			// builder.setStrictHostKeyChecking(fileSystemOptions, "no");
			// builder.setIdentityRepositoryFactory(fileSystemOptions, new TestIdentityRepositoryFactory());
			// builder.setUserInfo(fileSystemOptions, new TrustEveryoneUserInfo());
			builder.setRootURI(fileSystemOptions, rootUri);

			logger.info("read rootUri :{}.", rootUri);
			remote = manager.resolveFile(rootUri, fileSystemOptions);
			logger.info("read  3");
			// logger.info(" remote.getType() {}", remote.getType());
			// remote.getType()
			// logger.info("3a");
			var i = 0;
			var rmChildren = remote.getChildren();
			logger.info("read 3b");
			for (var chld : rmChildren) {
				i++;
				logger.info("read child {}   name {}", i, chld.getName());
			}

			logger.info("read done");
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("read ftp test " + e, e);
		} finally {

			try {
				logger.info("read 5");
				remote.close();
			} catch (FileSystemException e) {
				logger.error("read Sftp test remote close ", e);
			}
			com.btc.withings.ftp.FTPTest.slpAndExit();

		}

	}

}
