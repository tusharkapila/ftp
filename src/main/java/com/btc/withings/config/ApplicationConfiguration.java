package com.btc.withings.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import lombok.Data;

/**
 * Will pick up current profile property file. File in resources will be in war/jar, same can also be put outside jar, in same folder as jar for easy
 * over ride. Or in any other location in the classpath.
 */
@Configuration
@Data
public class ApplicationConfiguration {
	private static final Logger logger = LogManager.getLogger(ApplicationConfiguration.class);

	@Value("${withings.oauth2.url}")
	public String withingsOauth2Url;

	@Value("${withingsClientId}")
	public String withingsClientId;

	@Value("${withingsSecretKey}")
	public String withingsSecretKey;

	@Value("${withingsInitialSetupUrl}")
	public String withingsInitialSetupUrl;

	@Value("${redirectUri}")
	public String redirectUri;

	@Value("${fromEmailAddress}")
	public String fromEmailAddress;

	@Value("${fromEmailPasswod}")
	public String fromEmailPasswod;

	@Value("${sslFactoryValue}")
	public String sslFactoryValue;

	@Value("${smtpPortValue}")
	public String smtpPortValue;

	@Value("${smtpHostName}")
	public String smtpHostName;

	@Value("${mail.subject}")
	private String mailSubject;

	@Value("${userRegContent}")
	private String userRegContent;

	@Value("${emailVerifyCodeExpiry}")
	private Integer emailVerifyCodeExpiry;

	@Value("${pinNumberSize}")
	private Integer pinNumberSize;

	@Value("${emailVerifyContent}")
	private String emailVerifyContent;

	@Value("${forgotPwdContent}")
	private String forgotPwdContent;

	@Value("${withingsWebSetupUrl}")
	public String withingsWebSetupUrl;

	@Value("${withingsOauthUrl}")
	public String withingsOauthUrl;

	@Value("${withingsDeviceDetailsUrl}")
	public String withingsDeviceDetailsUrl;

	@Value("${withingsGetSignatureUrl}")
	public String withingsGetSignatureUrl;

	@Value("${withingsSubscribeNotificationUrl}")
	public String withingsSubscribeNotificationUrl;

	@Value("${withingsNotificationCallbackUri}")
	public String withingsNotificationCallbackUri;

	@Value("${e1}")
	public String varE1;

	@Value("${withingsSleepSummaryUrl}")
	public String withingsSleepSummaryUrl;

	@Value("${withingsActivityOrMeasureUrl}")
	public String withingsActivityOrMeasureUrl;

	@EventListener(ApplicationReadyEvent.class)
	public void afterSpringStartup() {
		logger.info("Starting, v0.2.1 vals :var E1 {}; withingsSubscribeNotificationUrl : {} ", varE1, withingsSubscribeNotificationUrl);
	}

}
