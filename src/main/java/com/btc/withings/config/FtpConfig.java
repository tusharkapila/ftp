package com.btc.withings.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
public class FtpConfig {
	@Value("${ftp.host}")
	private String host;

	@Value("${ftp.port:2}")
	private Integer port;

	@Value("${ftp.remoteDir}")
	private String remoteDir;

	@Value("${ftp.username}")
	private String username;

	@Value("${ftp.password}")
	private String password;

	@Value("${ftp.protocol}")
	private String protocol;

	@Value("${ftp.tryUpload:false}")
	private boolean tryUpload;

	@Value("${ftp.tryList:false}")
	private boolean tryList;

//    @Value("ftp.clientMode:2")
//    private String clientMode;

	/**
	 * Suggest to use BINARY so there is no change to file. Worls for text and binary files.
	 * ASCII format is good if want new lines to be chaged if different from source OS.
	 * See : (1) https://webmasters.stackexchange.com/a/104640/44492 and (2) https://superuser.com/a/4403/215555
	 */
//    @Value("ftp.fileType:2")
//    private Integer fileType;
//
//    @Value("ftp.useClientMode:true")
//    private boolean useClientMode;
//
//    @Value("ftp.cipherSuites:a,b,c")
//    private String cipherSuites;
//
//    @Value("ftp.protocol:SSL")
//    private String protocol;
//
//    @Value("ftp.trustManager")
//    private String trustManager;
//
//    @Value("ftp.prot")
//    private String prot;
//
//    @Value("ftp.needClientAuth")
//    private Boolean needClientAuth;
//
//    @Value("ftp.authValue")
//    private String authValue;
//
//    @Value("ftp.implicit:true")
//    private Boolean implicit;
//
//    @Value("ftp.sessionCreation:true")
//    private Boolean sessionCreation;
//
//    @Value("ftp.protocols:SSL, TLS")
//    private String protocols;

}
