package com.btc.withings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableScheduling
@EnableAutoConfiguration

public class FtpTestWebApplication {

	public static void main(String[] args) {
		for (String arg : args) {
			System.out.println("Arg " + arg);
		}
		// System.exit(0);
		SpringApplication.run(FtpTestWebApplication.class, args);
	}

}
