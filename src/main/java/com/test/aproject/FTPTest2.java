package com.test.aproject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.vfs2.provider.ftps.FtpsMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.btc.withings.config.FtpConfig;

@Service
public class FTPTest2 implements ApplicationListener<ApplicationReadyEvent> {
	@Autowired
	private FtpConfig ftpConfig;

	private static final Logger logger = LogManager.getLogger(FTPTest2.class);

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("ApplicationListener#onApplicationEvent FTPTest2 ()");

		String ftpServer = ftpConfig.getHost();
		String ftpUsername = ftpConfig.getUsername();
		String ftpPassword = ftpConfig.getPassword();

		FTPSClient ftp = null;
		InputStream fis = null;
		try {

			logger.info("1");
			ftp = new FTPSClient();
			ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
			var duraI = 70 * 1000;
			try {
				ftp.setConnectTimeout(duraI);
				ftp.setDataTimeout(duraI);
				ftp.setDefaultTimeout(duraI);

			} catch (Exception e) {
				logger.info("2ve err setConnectTimeout ", e);

			}

			/// ftp.setActivePortRange(0, 65000);
			ftp.connect(ftpServer, 21);
			try {
				ftp.setSoTimeout(duraI);
			} catch (Exception e) {
				logger.info("2ve err setSoTimeout ", e);

			}

			var fl = new File(new File("/d/tushar/www/"), "tst5.txt");

			logger.info("new fle {}", fl);
			//

			int reply = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				logger.warn("coult not connect to ftp server reply : {}", reply);

				return;

			}

			ftp.enterLocalPassiveMode();// Run the passive mode command now instead of after loggin in.
			ftp.login(ftpUsername, ftpPassword);
			ftp.execPBSZ(0);
			ftp.execPROT("P");
			ftp.type(FTP.BINARY_FILE_TYPE);
			reply = ftp.cwd(ftpConfig.getRemoteDir());
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				logger.warn("coult not cwd to  : {}, reply :", ftpConfig.getRemoteDir(), reply);
				return;

			}
			/// builder.setSessionTimeout(fileSystemOptions, 35000);

			// builder.setFileNameEncoding(fileSystemOptions, "UTF-8");
			var rootUri = ftpConfig.getProtocol() + "://" + ftpConfig.getUsername() + ":" + ftpConfig.getPassword() + "@" + ftpConfig.getHost() + ""
					+ ftpConfig.getRemoteDir();
			// builder.setStrictHostKeyChecking(fileSystemOptions, "no");
			// builder.setIdentityRepositoryFactory(fileSystemOptions, new TestIdentityRepositoryFactory());
			// builder.setUserInfo(fileSystemOptions, new TrustEveryoneUserInfo());

			var fileS = rootUri;
			logger.info("rootUri :{}, full File :{}. FtpsMode.EXPLICIT {}", rootUri, fileS, FtpsMode.EXPLICIT);

			logger.info("3");
			// logger.info(" remote.getType() {}", remote.getType());
			// remote.getType()
			// logger.info("3a");
//			var i = 0;
//			var rmChildren = remote.getChildren();
//			logger.info("3b");
//			for (var chld : rmChildren) {
//				i++;
//				logger.info("child {} type {} , name {}", i, chld.getType(), chld.getName());
//			}
//			logger.info("list files");
//			FTPFile[] ftpFiles = ftp.listFiles();
//			System.out.println("---------->Number of Files = " + ftpFiles.length);
//			int i = 0;
//			for (FTPFile fFile : ftpFiles) {
//				i++;
//				logger.info(" remote {} file {}, raw {}.", i, fFile.getName(), fFile.getRawListing());
//			}
			logger.info("attempt upload, open local stream");

			fis = new BufferedInputStream(new FileInputStream(fl));
			logger.info("storeFile");
			ftp.storeFile(fl.getName(), fis);
			logger.info("completePendingCommand");
			ftp.completePendingCommand();
			logger.info("uploaded");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Sftp test " + e, e);
		} finally {
			try {
				logger.info("ftp disconnect");
				ftp.disconnect();
			} catch (Exception e) {
				logger.error("Sftp test local close ", e);
			}

			if (fis != null) {
				try {

					logger.info("fis close 4v");
					fis.close();

				} catch (Exception e) {
					logger.error("fis close 4 err ", e);
				}
			}
			if (ftp != null) {
				try {

					logger.info("ftp logout 5");
					ftp.logout();

				} catch (Exception e) {
					logger.error("ftp logout err ", e);
				}
			}
//			if (manager != null) {
//				try {
//					manager.close();
//				} catch (Exception e) {
//					logger.error("Sftp test mgr close ", e);
//				}
//
//			}
			com.btc.withings.ftp.FTPTest.slpAndExit();

		}

	}

}
