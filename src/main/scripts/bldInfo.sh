#!/bin/sh
date >./src/main/resources/bldInfo.txt
echo "" >>./src/main/resources/bldInfo.txt
echo "Status:" >>./src/main/resources/bldInfo.txt
git status >>./src/main/resources/bldInfo.txt
echo "" >>./src/main/resources/bldInfo.txt
echo "Last 5 commits :" >>./src/main/resources/bldInfo.txt
git log -n 5 >>./src/main/resources/bldInfo.txt
echo "" >>./src/main/resources/bldInfo.txt
echo "Okay End" >>./src/main/resources/bldInfo.txt


echo "Post build target directory contents:" >./src/main/resources/bldInfo2.txt
echo "" >>./src/main/resources/bldInfo2.txt
ls -al target/ >>./src/main/resources/bldInfo2.txt
echo "---" >>./src/main/resources/bldInfo2.txt
date >>./src/main/resources/bldInfo2.txt
echo "ok End." >>./src/main/resources/bldInfo2.txt
export spring_profiles_active=local
export server_port=8091


mvn clean install  -Dmaven.test.skip=true -Dspring.profiles.active=dev

echo "Post build target directory contents:" >./src/main/resources/bldInfo2.txt
echo "" >>./src/main/resources/bldInfo2.txt
ls -al target/ >>./src/main/resources/bldInfo2.txt
echo "---" >>./src/main/resources/bldInfo2.txt
date >>./src/main/resources/bldInfo2.txt
echo "ok End." >>./src/main/resources/bldInfo2.txt

mvn -q  package  -Dmaven.test.skip=true -Dspring.profiles.active=dev
