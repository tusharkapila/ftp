package com.btc.withings.model.settings;

import javax.persistence.*;

import lombok.Data;

@Data
@Table(name = "Settings_Scheduler_Run_Timing")
@Entity
public class RunTiming {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column
	private Integer hour;
	@Column
	private Integer minute;

	public RunTiming() {

	}

	public RunTiming(Integer h, Integer m) {
		hour = h;
		minute = m;
	}
}
