package com.btc.withings.model.settings;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "Settings_Workflow")
@Entity
public class WorkflowSettings {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "environment")
	private String environment;// default and then local,dev,uat,prod etc
	private List<RunTiming> runTimings = new ArrayList<>(); // Different times to run, once a day or more

//	//public void addRunTiming(RunTiming rt) {
//		runTimings.add(rt);
//	}

}
