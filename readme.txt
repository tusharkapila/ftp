mvn clean install  -Dmaven.test.skip=true -Dspring.profiles.active=dev

export spring_profiles_active=local
export server_port=8091
java -Dname=WiLocal$server_port -Dserver.port=$server_port   -Dspring.profiles.active=local  -jar target/withingswebservice-v2.jar

-Dspring.profiles.active="local,ftpTest"
export spring_profiles_active="local,ftpTest"
export server_port=8091
java -Dname=WiLocal$server_port -Dserver.port=$server_port   -Dspring.profiles.active=local  -jar target/withingswebservice-v2.jar
java -Dname=FtpTest$server_port -Dserver.port=$server_port   -Dspring.profiles.active="local,ftpTest"  -jar target/FtpTestWebApplication-v1.jar


 rm -r -f ./logs/*
 cat logs/catalina.out
export CATALINA_OPTS=-Dspring.profiles.active=dev

bin/catalina.sh stop
bin/catalina.sh start

# code checkout dev
cd /d/wi/withingsws

# code checkout uat
cd /d/wUat/wc

nohup java -Dname=WiDev1 -Dserver.port=8091 -Dspring.profiles.active=dev  -jar target/withingswebservice-v2.jar >out1 2>&1  & 


--spring.config.location=file:///   -jar target/withingswebservice-v2.jar


-----

You can use awk to a get first a column of ps output.

ps -A | grep <application_name> | awk '{print $1}' | xargs -n1

ps -A | grep java | awk '{print $1}' | xargs -n1
ps aux | grep "tomcat" | awk '{print $2}' | xargs -n1

Will return list of PIDs

19440
21630
22694

And adding kill -9 $1 you have a command which kills all PIDs

ps -A | grep <application_name> | awk '{print $1}' | xargs kill -9 $1
ps aux | grep "\-Dname=Wi" | awk '{print $2}' | xargs kill -9 $1

ps aux | grep "tomcat" | awk '{print $2}' | xargs kill -9 $1


----

http://3.140.203.223/dev91/withingswebservice/api/notifications/v1/ping2?f=6&n=m7
https://3.140.203.223:8443/dev91/withingswebservice/api/notifications/v1/ping2?f=6&n=m7

https://www.karyompntest.com/withingswebservice/index.html#/common-controller

UAT
https://www.karyompntest.com/withingswebservice/ping?f=6&n=m7

DEV
https://www.karyompntest.com/dev91/withingswebservice/api/notifications/v1/ping2?f=6&n=m7

---
